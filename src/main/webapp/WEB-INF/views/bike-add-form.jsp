<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<head>
    <title>Сервис проката велосипедов</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<br>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <form action="${pageContext.request.contextPath}/add" method="post">
                <caption>
                    <h2>
                        <c:if test="${bike == null}">
                            Добавить
                        </c:if>
                    </h2>
                </caption>

                <fieldset class="form-group">
                    <label>Название</label>
                    <input type="text" value="<c:out value='${bike.bikeName}' />" class="form-control" name="bikeName"
                           required="required">
                </fieldset>

                <fieldset class="form-group">
                    <label>Размер</label>
                    <input type="text" value="<c:out value='${bike.bikeSize}' />" class="form-control" name="bikeSize">
                </fieldset>

                <fieldset class="form-group">
                    <label>Цвет</label>
                    <input type="text" value="<c:out value='${bike.bikeColor}' />" class="form-control"
                           name="bikeColor">
                </fieldset>

                <fieldset class="form-group">
                    <label>Цена</label>
                    <input type="text" value="<c:out value='${bike.bikePrice}' />" class="form-control"
                           name="bikePrice">
                </fieldset>

                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>
</div>
</body>

</html>
