<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <title>Сервис проката велосипедов</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<br>

<div class="row">
    <div class="container">
        <h3 class="text-center">Администраторский раздел</h3>
        <hr>
        <div class="container text-left">
            <a href="<%=request.getContextPath()%>/add" class="btn btn-success">Добавить новый велосипед</a>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Название</th>
                <th>Размер</th>
                <th>Цвет</th>
                <th>Цена</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="bike" items="${listBike}">

                <tr>
                    <td>
                        <c:out value="${bike.bikeName}"/>
                    </td>
                    <td>
                        <c:out value="${bike.bikeSize}"/>
                    </td>
                    <td>
                        <c:out value="${bike.bikeColor}"/>
                    </td>
                    <td>
                        <c:out value="${bike.bikePrice}"/>
                    </td>
                    <td>
                        <button>
                            <a href="${pageContext.request.contextPath}/edit?id=<c:out value='${bike.id}' />">Редактировать</a>
                        </button>
                        <button>
                            <a href="${pageContext.request.contextPath}/delete?id=<c:out value='${bike.id}' />">Удалить</a>
                        </button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </div>
</div>
</body>

</html>