<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <title>Сервис проката велосипедов</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<br>

<div class="row">
    <div class="container">
        <h3 class="text-center">Список велосипедов для аренды</h3>
        <hr>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Название</th>
                <th>Размер</th>
                <th>Цвет</th>
                <th>Цена</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="bike" items="${listBike}">

                <tr>
                    <td>
                        <c:out value="${bike.bikeName}"/>
                    </td>
                    <td>
                        <c:out value="${bike.bikeSize}"/>
                    </td>
                    <td>
                        <c:out value="${bike.bikeColor}"/>
                    </td>
                    <td>
                        <c:out value="${bike.bikePrice}"/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </div>
</div>
</body>

</html>