<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <title>Сервис проката велосипедов</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<br>

<div class="row">
    <div class="container">
        <h3 class="text-center">Добро пожаловать на наш сайт</h3>
        <hr>
        <div class="container">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab assumenda blanditiis consequatur dignissimos
            doloribus eligendi inventore itaque laboriosam nam quidem quos, temporibus! Assumenda corporis enim eveniet
            illo incidunt, itaque unde. Aliquid consequuntur delectus doloremque eos error esse harum molestiae
            molestias nemo nesciunt obcaecati qui quia, reiciendis sit tempora, temporibus voluptatem. Lorem ipsum dolor
            sit amet, consectetur adipisicing elit. Architecto at doloribus error ipsam, nihil quaerat repellat veniam
            voluptate? Alias autem debitis eligendi error, explicabo facilis neque, praesentium recusandae reiciendis
            repellat rerum saepe sint ut veritatis voluptate. Eaque eum illo incidunt ipsa minus nulla officia quasi sed
            tenetur! Numquam, repudiandae, voluptas.
        </div>
        
    </div>
</div>
</body>

</html>
