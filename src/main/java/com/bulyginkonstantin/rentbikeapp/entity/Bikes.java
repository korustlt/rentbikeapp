package com.bulyginkonstantin.rentbikeapp.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author konst
 */
@Entity
@Table(name = "bikes")
@NamedQueries({
    @NamedQuery(name = "Bikes.findAll", query = "SELECT b FROM Bikes b"),
    @NamedQuery(name = "Bikes.findById", query = "SELECT b FROM Bikes b WHERE b.id = :id"),
    @NamedQuery(name = "Bikes.findByBikeName", query = "SELECT b FROM Bikes b WHERE b.bikeName = :bikeName"),
    @NamedQuery(name = "Bikes.findByBikeSize", query = "SELECT b FROM Bikes b WHERE b.bikeSize = :bikeSize"),
    @NamedQuery(name = "Bikes.findByBikeColor", query = "SELECT b FROM Bikes b WHERE b.bikeColor = :bikeColor"),
    @NamedQuery(name = "Bikes.findByBikePrice", query = "SELECT b FROM Bikes b WHERE b.bikePrice = :bikePrice")})
public class Bikes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "bike_name")
    private String bikeName;
    @Basic(optional = false)
    @Column(name = "bike_size")
    private double bikeSize;
    @Basic(optional = false)
    @Column(name = "bike_color")
    private String bikeColor;
    @Basic(optional = false)
    @Column(name = "bike_price")
    private double bikePrice;

    public Bikes() {
    }

    public Bikes(Integer id) {
        this.id = id;
    }

    public Bikes(Integer id, String bikeName, double bikeSize, String bikeColor, double bikePrice) {
        this.id = id;
        this.bikeName = bikeName;
        this.bikeSize = bikeSize;
        this.bikeColor = bikeColor;
        this.bikePrice = bikePrice;
    }

    public Bikes(String bikeName, double bikeSize, String bikeColor, double bikePrice) {
        this.bikeName = bikeName;
        this.bikeSize = bikeSize;
        this.bikeColor = bikeColor;
        this.bikePrice = bikePrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBikeName() {
        return bikeName;
    }

    public void setBikeName(String bikeName) {
        this.bikeName = bikeName;
    }

    public double getBikeSize() {
        return bikeSize;
    }

    public void setBikeSize(double bikeSize) {
        this.bikeSize = bikeSize;
    }

    public String getBikeColor() {
        return bikeColor;
    }

    public void setBikeColor(String bikeColor) {
        this.bikeColor = bikeColor;
    }

    public double getBikePrice() {
        return bikePrice;
    }

    public void setBikePrice(double bikePrice) {
        this.bikePrice = bikePrice;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bikes)) {
            return false;
        }
        Bikes other = (Bikes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.mavenproject3.entity.Bikes[ id=" + id + " ]";
    }

}
