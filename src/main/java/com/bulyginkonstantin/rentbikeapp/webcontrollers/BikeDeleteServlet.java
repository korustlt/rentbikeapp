package com.bulyginkonstantin.rentbikeapp.webcontrollers;

import com.bulyginkonstantin.rentbikeapp.entity.Bikes;
import com.bulyginkonstantin.rentbikeapp.sessions.BikesFacade;
import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Аннотированный сервлет, обрабатывает запросы на удаление велосипеда из базы
 * данных
 */
@WebServlet(name = "BikeDeleteServlet", urlPatterns = "/delete")
public class BikeDeleteServlet extends HttpServlet {

    /**
     * Уникальный номер для сервлета
     */
    private static final long serialVersionUID = 4L;

    @Inject
    private BikesFacade bikesFacade;

    /**
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer id = Integer.valueOf(request.getParameter("id"));
        Bikes bike = bikesFacade.find(id);
        bikesFacade.remove(bike);
        List<Bikes> listBike = bikesFacade.findAll();
        request.setAttribute("listBike", listBike);
        request.getRequestDispatcher("/WEB-INF/views/bike-admin-list.jsp").forward(request, response);
    }

}
