package com.bulyginkonstantin.rentbikeapp.webcontrollers;

import com.bulyginkonstantin.rentbikeapp.entity.Bikes;
import com.bulyginkonstantin.rentbikeapp.sessions.BikesFacade;
import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Аннотированный сервлет, обрабатывает запросы на изменение велосипеда в базе
 * данных
 */
@WebServlet(name = "BikeEditFormServlet", urlPatterns = "/edit")
public class BikeEditFormServlet extends HttpServlet {

    /**
     * Уникальный номер для сервлета
     */
    private static final long serialVersionUID = 5L;

    @Inject
    private BikesFacade bikesFacade;

    /**
     * @param request
     * @param response
     * @throws jakarta.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            // получение парамметров запроса
            Integer id = Integer.valueOf(request.getParameter("id"));
            String bikeName = request.getParameter("bikeName");
            Double bikeSize = Double.valueOf(request.getParameter("bikeSize"));
            String bikeColor = request.getParameter("bikeColor");
            Double bikePrice = Double.valueOf(request.getParameter("bikePrice"));
            // создание объекта Bike
            Bikes bike = new Bikes(id, bikeName, bikeSize, bikeColor, bikePrice);
            bikesFacade.edit(bike);
            List<Bikes> listBike = bikesFacade.findAll();
            request.setAttribute("listBike", listBike);
            // перенаправляет на страницу с администрирования
            request.getRequestDispatcher("/WEB-INF/views/bike-admin-list.jsp").forward(request, response);
        } catch (ServletException | IOException | NumberFormatException err) {
            System.out.println("Number format exception " + err.getMessage());
            request.getRequestDispatcher("/WEB-INF/views/bike-edit-form.jsp").forward(request, response);
        }

    }

    /**
     * @param request
     * @param response
     * @throws jakarta.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer id = Integer.valueOf(request.getParameter("id"));
        Bikes bike = bikesFacade.find(id);
        request.setAttribute("bike", bike);
        // перенаправляет на страницу с формой редактирования велосипеда
        request.getRequestDispatcher("/WEB-INF/views/bike-edit-form.jsp").forward(request, response);
    }
}
