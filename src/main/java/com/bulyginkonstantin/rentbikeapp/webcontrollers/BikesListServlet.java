package com.bulyginkonstantin.rentbikeapp.webcontrollers;

import com.bulyginkonstantin.rentbikeapp.entity.Bikes;
import com.bulyginkonstantin.rentbikeapp.sessions.BikesFacade;
import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Аннотированный сервлет, обрабатывает запросы отображение списка велосипедов
 * для пользователей
 */
@WebServlet(name = "BikesListServlet", urlPatterns = "/bikes")
public class BikesListServlet extends HttpServlet {


    @Inject
    private BikesFacade bikesFacade;

    @Override
    public void init() throws ServletException {
        getServletContext().setAttribute("listBike", bikesFacade.findAll());
    }

    /**
     * Уникальный номер для сервлета
     */
    private static final long serialVersionUID = 6L;

    /**
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Bikes> listBike = bikesFacade.findAll();
        request.setAttribute("listBike", listBike);
        getServletContext().getRequestDispatcher("/WEB-INF/views/bike-list.jsp").forward(request, response);
    }
}
