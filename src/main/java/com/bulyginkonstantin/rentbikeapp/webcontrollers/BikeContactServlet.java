package com.bulyginkonstantin.rentbikeapp.webcontrollers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Аннотированный сервлет, обрабатывает запросы для страницы контактов
 */
@WebServlet(name = "BikeContactServlet", urlPatterns = "/contact")
public class BikeContactServlet extends HttpServlet {

    /**
     * Уникальный номер для сервлета
     */
    private static final long serialVersionUID = 3L;


    /**
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/bike-contact.jsp").forward(request, response);
    }

}