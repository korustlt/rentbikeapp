package com.bulyginkonstantin.rentbikeapp.webcontrollers;


import com.bulyginkonstantin.rentbikeapp.entity.Bikes;
import com.bulyginkonstantin.rentbikeapp.sessions.BikesFacade;
import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Аннотированный сервлет, обрабатывает запросы на добавление велосипеда в базу
 * данных
 */
@WebServlet(name = "BikeAddFormServlet", urlPatterns = "/add")
public class BikeAddFormServlet extends HttpServlet {

    /**
     * Уникальный номер для сервлета
     */
    private static final long serialVersionUID = 1L;

    @Inject
    private BikesFacade bikesFacade;

    /**
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            // получение парамметров запроса
            String bikeName = request.getParameter("bikeName");
            Double bikeSize = Double.valueOf(request.getParameter("bikeSize"));
            String bikeColor = request.getParameter("bikeColor");
            Double bikePrice = Double.valueOf(request.getParameter("bikePrice"));

            Bikes newBike = new Bikes(bikeName, bikeSize, bikeColor, bikePrice);
            bikesFacade.create(newBike);
            List<Bikes> listBike = bikesFacade.findAll();
            request.setAttribute("listBike", listBike);
            request.getRequestDispatcher("/WEB-INF/views/bike-admin-list.jsp").forward(request, response);

        } catch (ServletException | IOException | NumberFormatException err) {
            System.out.println("Number format exception " + err.getMessage());
            request.getRequestDispatcher("/WEB-INF/views/bike-add-form.jsp").forward(request, response);
        }

    }

    /**
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/bike-add-form.jsp").forward(request, response);
    }
}
