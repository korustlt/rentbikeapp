package com.bulyginkonstantin.rentbikeapp.sessions;


import com.bulyginkonstantin.rentbikeapp.entity.Bikes;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import javax.ejb.Stateless;


/**
 *
 * @author konst
 */
@Stateless
public class BikesFacade extends AbstractFacade<Bikes> implements BikesFacadeLocal {

    @PersistenceContext(unitName = "rentBikeUP")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BikesFacade() {
        super(Bikes.class);
    }
    
}
