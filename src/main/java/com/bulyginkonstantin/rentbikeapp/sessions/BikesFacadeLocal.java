package com.bulyginkonstantin.rentbikeapp.sessions;

import com.bulyginkonstantin.rentbikeapp.entity.Bikes;
import jakarta.ejb.Local;
import java.util.List;


/**
 *
 * @author konst
 */
@Local
public interface BikesFacadeLocal {

    void create(Bikes bikes);

    void edit(Bikes bikes);

    void remove(Bikes bikes);

    Bikes find(Object id);

    List<Bikes> findAll();

    List<Bikes> findRange(int[] range);

    int count();
    
}
